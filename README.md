# FileBasedDataStructures

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://trenta3.gitlab.io/FileBasedDataStructures.jl/dev)
[![Build Status](https://gitlab.com/trenta3/FileBasedDataStructures.jl/badges/master/pipeline.svg)](https://gitlab.com/trenta3/FileBasedDataStructures.jl/pipelines)
[![Coverage](https://gitlab.com/trenta3/FileBasedDataStructures.jl/badges/master/coverage.svg)](https://gitlab.com/trenta3/FileBasedDataStructures.jl/commits/master)
[![Codecov](https://codecov.io/gh/trenta3/FileBasedDataStructures.jl/branch/master/graph/badge.svg)](https://codecov.io/gh/trenta3/FileBasedDataStructures.jl)

A package to freely use data structures that are saved to disk at every operation.
