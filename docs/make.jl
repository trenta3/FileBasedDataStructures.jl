using Documenter, FileBasedDataStructures

makedocs(;
    modules=[FileBasedDataStructures],
    format=Documenter.HTML(),
    pages=[
        "Home" => "index.md",
    ],
    repo="https://gitlab.com/trenta3/FileBasedDataStructures.jl/blob/{commit}{path}#L{line}",
    sitename="FileBasedDataStructures.jl",
    authors="Dario Balboni",
    assets=String[],
)
